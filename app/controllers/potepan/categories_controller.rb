class Potepan::CategoriesController < ApplicationController
  def show
    taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.includes(master: [:default_price, :images]).in_taxon(taxon)
    @breadcrumbs_name = get_pankuzu(taxon)
  end

  private

  def get_pankuzu(taxon)
    breadcrumbs = [taxon]
    breadcrumbs_name = [taxon.name]
    taxon.depth.times do
      breadcrumbs.unshift(breadcrumbs.first.parent)
      breadcrumbs_name.unshift(breadcrumbs.first.name)
    end
    breadcrumbs_name.unshift('Home', 'Shop')
  end
end
