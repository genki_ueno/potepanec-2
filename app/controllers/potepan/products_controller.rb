class Potepan::ProductsController < ApplicationController
  def show
    @product          = Spree::Product.find(params[:id])
    @breadcrumbs_name = ['Home', @product.name]
  end
end
