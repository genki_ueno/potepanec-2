require 'rails_helper'

describe 'カテゴリー絞り込み機能', type: :system do
  describe '商品表示機能' do
    let!(:taxonomy) { create(:taxonomy, name: 'Taxonomy', taxons: [grand_parent_taxon]) }
    let(:taxon) { create(:taxon, parent_id: parent_taxon.id) }
    let(:other_taxon) { create(:taxon, name: 'React Native') }
    let(:parent_taxon) { create(:taxon, name: 'parent_taxon', parent_id: grand_parent_taxon.id) }
    let(:grand_parent_taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon]) }

    before do
      taxon.taxonomy = taxonomy
      visit potepan_category_path(taxon.id)
    end

    context 'カテゴリーを1つ選択' do
      it '選択されているカテゴリーの名前が表示されている' do
        expect(page).to have_content(taxon.name)
      end

      it '選択されているカテゴリーの親カテゴリーが表示されている' do
        expect(page).to have_content(taxon.parent.name)
      end

      it '選択されているカテゴリーのrootカテゴリー(Taxonomy)が表示されている' do
        expect(page).to have_content(taxon.taxonomy.name)
      end

      it '別のカテゴリーが表示されていない' do
        expect(page).not_to have_content(other_taxon.name)
      end

      it '選択されたカテゴリーの商品が表示されている' do
        expect(page).to have_content(product.name)
      end

      it '選択していないカテゴリーの商品が表示されない' do
        expect(page).not_to have_content(other_product.name)
      end
    end

    context '左サイドバーカテゴリーを選択' do
      it 'クリック前は子カテゴリーが表示されていない', js: true do
        within '.side-nav' do
          expect(page).not_to have_content(taxon.name)
        end
      end

      it 'クリック後は子カテゴリーが表示されている', js: true do
        page.all(".rspec-taxonomy-tag")[1].click
        within '.side-nav' do
          expect(page).to have_content(taxon.name)
        end
      end

      it '子カテゴリーをクリック後、そのカテゴリーの商品が表示されている', js: true do
        page.all(".rspec-taxonomy-tag")[1].click
        first(".rspec-child-taxon-tag").click
        expect(page).to have_content(product.name.upcase)
        expect(page).not_to have_content(other_product.name.upcase)
      end
    end
  end
end
