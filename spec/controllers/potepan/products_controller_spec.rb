require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has set correct values" do
      expect(assigns(:product)).to eq(product)
    end

    it "render the :show template" do
      expect(response).to render_template :show
    end
  end
end
